package com.testing.crudtest.adapter


import androidx.room.*
import com.testing.crudtest.model.Note

@Dao
interface NoteDao {

    @Insert
    fun insert(note: Note)

    @Update
    fun update(note: Note)

    @Delete
    fun delete(note: Note)

    @Query("SELECT * FROM datastaff")
    fun getAll() : List<Note>

    @Query("SELECT * FROM datastaff WHERE id = :id")
    fun getById(id: Int) : List<Note>

    @Query("DELETE FROM datastaff WHERE id = :id")
    fun deleteById(id: Int)
}