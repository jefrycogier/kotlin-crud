package com.testing.crudtest

import android.content.ContentValues
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.testing.crudtest.adapter.NoteDbManager
import kotlinx.android.synthetic.main.activity_note.*
import org.jetbrains.anko.startActivity

class NoteActivity : AppCompatActivity() {

    var id = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note)

        try {
            var bundle = Bundle()
            //bundle.putString("data1", edtTitle.text.toString())

            //var bundle: Bundle = intent.extras
            //id = bundle.getInt("MainActId", 0)
            //if (id != 0) {
            //    edtTitle.setText(bundle.getString("MainActTitle"))
            //    edtContent.setText(bundle.getString("MainActContent"))
            //}
            val extras = intent.extras
            val id = extras!!.getInt("MainActId")
            if (id != 0) {
                edtTitle.setText(extras!!.getString("MainActTitle"))
                edtContent.setText(extras!!.getString("MainActContent"))
            }

        } catch (ex: Exception) {
        }

        btAdd.setOnClickListener {
            var dbManager = NoteDbManager(this)

            var values = ContentValues()
            values.put("Title", edtTitle.text.toString())
            values.put("Content", edtContent.text.toString())

            if (id == 0) {
                val mID = dbManager.insert(values)

                if (mID > 0) {
                    Toast.makeText(this, "Add note successfully!", Toast.LENGTH_LONG).show()
                    //startActivity(Intent(this, RoomActivity::class.java))
                    startActivity<RoomActivity>()
                } else {
                    Toast.makeText(this, "Fail to add note!", Toast.LENGTH_LONG).show()
                }
            } else {
                var selectionArs = arrayOf(id.toString())
                val mID = dbManager.update(values, "Id=?", selectionArs)

                if (mID > 0) {
                    Toast.makeText(this, "Add note successfully!", Toast.LENGTH_LONG).show()
                    //startActivity(Intent(this, RoomActivity::class.java))
                    startActivity<RoomActivity>()
                } else {
                    Toast.makeText(this, "Fail to add note!", Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}
