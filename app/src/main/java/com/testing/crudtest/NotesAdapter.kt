package com.testing.crudtest

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.testing.crudtest.model.Note

class NotesAdapter(
    private val listItems: ArrayList<Note>,
    private val listener: NoteListener
) : RecyclerView.Adapter<NotesAdapter.NoteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        return NoteViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val item = listItems[position]
        holder.textViewName.text = item.name
        holder.textViewPositions.text = item.positions
        holder.textViewStatus.text = item.status
        holder.textViewAddress.text = item.address
        holder.itemView.setOnClickListener {
            listener.OnItemClicked(item)
        }
    }

    class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewName = itemView.findViewById<TextView>(R.id.text_view_name)
        var textViewPositions = itemView.findViewById<TextView>(R.id.text_view_positions)
        var textViewStatus = itemView.findViewById<TextView>(R.id.text_view_status)
        var textViewAddress = itemView.findViewById<TextView>(R.id.text_view_alamat)
    }

    interface NoteListener{
        fun OnItemClicked(note: Note)
    }
}

