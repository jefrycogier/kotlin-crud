package com.testing.crudtest

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.testing.crudtest.adapter.NoteRoomDatabase
import com.testing.crudtest.model.Note
import kotlinx.android.synthetic.main.activity_rooms.*




class MaintestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rooms)

        getNotesData()

        floatingActionButton.setOnClickListener {
            startActivity(Intent(this, EditActivity::class.java))
        }

    }

    private fun getNotesData(){
        val database = NoteRoomDatabase.getDatabase(applicationContext)
        val dao = database.getNoteDao()
        val listItems = arrayListOf<Note>()
        listItems.addAll(dao.getAll())
        setupRecyclerView(listItems)
        if (listItems.isNotEmpty()){
            text_view_note_empty.visibility = View.GONE
        }
        else{
            text_view_note_empty.visibility = View.VISIBLE
        }
    }

    private fun setupRecyclerView(listItems: ArrayList<Note>){
        recycler_view_main.apply {
            adapter = NotesAdapter(listItems, object : NotesAdapter.NoteListener{
                override fun OnItemClicked(note: Note) {

                    var intent = Intent(this@MaintestActivity, EditActivity::class.java)
                    intent.putExtra("MainActId", note.id)
                    intent.putExtra("MainActName", note.name)
                    intent.putExtra("MainActPositions", note.positions)
                    intent.putExtra("MainActStatus", note.status)
                    intent.putExtra("MainActAddress", note.address)
                    Log.i("test mainact", "klik, position: " + note.id)
                    startActivity(intent)

                    /*val intent = Intent(this@MaintestActivity, EditActivity::class.java)
                    intent.putExtra(EditActivity().GET_DATA, note)
                    startActivity(intent)*/
                }
            })

            layoutManager = LinearLayoutManager(this@MaintestActivity)
        }
    }

    override fun onResume() {
        super.onResume()
        getNotesData()
    }
}
