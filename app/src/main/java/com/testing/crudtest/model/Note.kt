package com.testing.crudtest.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

//Entity annotation to specify the table's name
@Entity(tableName = "datastaff")
//Parcelable annotation to make parcelable object
@Parcelize
data class Note(
    //PrimaryKey annotation to declare primary key
    //ColumnInfo annotation to specify the column's name
    @PrimaryKey(autoGenerate = true)@ColumnInfo(name = "id") var id: Int = 0,
    @ColumnInfo(name = "name") var name: String = "",
    @ColumnInfo(name = "positions") var positions: String = "",
    @ColumnInfo(name = "status") var status: String = "",
    @ColumnInfo(name = "address") var address: String = ""
) : Parcelable {
}